#!/bin/sh

sed -i -e 's/charset=\"UTF-8\"/charset=\"ISO-8859-1\"/' public/index.html
iconv -f UTF-8 -t ISO-8859-1 public/index.html > public/_index.html
mv public/_index.html public/index.html
sed -i -e 's/charset=\"UTF-8\"/charset=\"ISO-8859-1\"/' public/en/index.html
iconv -f UTF-8 -t ISO-8859-1 public/en/index.html > public/en/_index.html
mv public/en/_index.html public/en/index.html
