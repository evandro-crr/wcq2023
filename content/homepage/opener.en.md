---
title: 'About'
weight: 10
---

With great enthusiasm, we announce the latest edition of the Quantum Computing Workshop. This event brings together renowned experts from academia and industry, with the purpose of keeping the Brazilian community updated on the rapid advancements in the field of Quantum Computing. This revolution is based on the application of quantum mechanics to create a new logic of information processing.

Despite being in its early stages, quantum processors [Jiuzhang](https://www.science.org/doi/10.1126/science.abe8770), [Jiuzhang 2.0](https://doi.org/10.1103/PhysRevLett.127.180502), and [Zuchongzhi](https://doi.org/10.1103/PhysRevLett.127.180501), all from China, along with the notable photonic processor [Borealis](https://www.nature.com/articles/s41586-022-04725-x) developed by the esteemed Canadian company Xanadu, have already surpassed the processing power of renowned supercomputers like Summit and Sunway TaihuLight. In some cases, quantum computers have demonstrated speeds up to 10 million billion times faster than their classical counterparts in exact simulations!

The [Quantum Computing Group of UFSC](http://www.gcq.ufsc.br/) is honored to invite you to join us in this exciting scientific and technological revolution. Throughout the event, we will offer lectures and workshops to present the latest achievements in this dynamic field. Below, we provide more information about the event and how you can actively participate in this journey towards the future of computing.

<br>

### [Sign Up Now!](#inscrição) (Free)

Stay tuned to our [Instagram](https://www.instagram.com/gcq_ufsc/) and [Youtube](https://www.youtube.com/@GCQUFSC) and don't miss the opportunity to sign up and participate in this transformative event. Get ready to join us on this journey of discoveries and advancements in Quantum Computing. Your presence is crucial to further enrich this unique experience. We eagerly await your registration!

<br>

### How to Participate

Participating in the event is simple! All you need to do is [sign up](#inscrição) to secure your spot. Once registered, you'll have two options to follow the lectures:

1. **Live on YouTube:** Tune in to the exciting talks from wherever you are, through live broadcasts on our official YouTube channel. You'll have the opportunity to interact through comments and directly absorb knowledge from experts in the field of Quantum Computing.

2. **In Person:** If you prefer a more immersive experience, we invite you to join us in person at the event venue. Check the schedule for details on the timings and locations of the lectures. We look forward to welcoming you to an inspiring environment, where you can directly connect with fellow enthusiasts and delve into revolutionary discussions.

Remember to check the complete schedule for information about timings, locations, and speakers. This is your opportunity to engage with the forefront of Quantum Computing and expand your horizons in this ever-evolving field. We can't wait to share this journey with you!

<br>

### Previous Editions

As we prepare for the exciting sixth edition of the Quantum Computing Workshop, it's worth reflecting on the previous editions that have brought us to this point:

- [V Quantum Computing Workshop (2022)](https://workshop-cq.ufsc.br/2022/)
- [IV Quantum Computing Workshop (2021)](https://workshop-cq.ufsc.br/2021/)
- [III Quantum Computing Workshop (2020)](https://workshop-cq.ufsc.br/2020/)
- [II Quantum Computing Workshop (2019)](https://workshop-cq.ufsc.br/2019/)
- [I Quantum Computing Workshop (2018)](https://workshop-cq.ufsc.br/2018/)

<style>
    body { text-align: justify }
</style>