---
title: 'Palestrantes'
weight: 50
header_menu: true
---

--------------------------------------

# Adenilton José da Silva
*Universidade Federal de Pernambuco (Brasil)*

![Adenilton José da Silva](fotos/adenilton.jpg)

<p align="justify">
Adenilton Silva é professor do Centro de Informática da UFPE desde 2019. Seus principais interesses de pesquisa são software de sistema para dispositivos quânticos, aprendizagem de máquina quântica e algoritmos.
</p>

--------------------------------------

# Bruno Taketani
*IQM Computadores Quânticos (Alemanha)*

![Bruno Taketani](fotos/bruno.jpg)

<p align="justify">
Bruno G. Taketani trabalha na IQM Quantum Computers como Gerente Técnico de Produtos, desenvolvendo a integração do sistema da empresa em ambientes de computação de alto desempenho (HPC). Seu trabalho em computação quântica teve início durante seu doutorado, onde se concentrou no desenvolvimento de protocolos para a era NISQ. Em seguida, passou 5 anos como pesquisador na Alemanha, trabalhando em algoritmos quânticos e aplicações. Antes de ingressar na IQM, Bruno ocupou um cargo de professor na UFSC, Brasil, onde contribuiu para fortalecer os esforços locais em tecnologias quânticas. Bruno também trabalha na construção do ecossistema europeu no Consórcio da Indústria Quântica Europeia (QuIC) e representa esse grupo em um grupo consultivo da EuroHPC Joint Undertaken.
</p>

--------------------------------------

# Celso Villas-Boas
*Universidade Federal de São Carlos (Brasil)*

![Celso Villas-Boas](fotos/celso.jpg)

<p align="justify">
Celso J. Villas-Boas: É professor associado da UFSCar, São Carlos. Foi chefe do Departamento de Física, coordenador do curso de graduação e de pós-graduação em Física/UFSCar. Tem mais de 25 anos de experiência em Óptica e Informação Quântica, em especial: processamento de informação quântica, computação quântica e interação radiação-matéria. Já orientou mais de 50 projetos de IC, mestrado, doutorado e pós-doutorado, e é coautor de importantes trabalhos na área, por exemplo, o primeiro a demonstrar a robustez da discórdia quântica, o primeiro a demonstrar a transparência induzida eletromagneticamente com átomos individuais; o primeiro a demonstrar que a geração de correlações entre átomos funciona como testemunha da não classicalidade de campos ópticos intensos. Atualmente coordena projetos em tecnologias quânticas de segunda geração junto a órgãos públicos e privados.
</p>

--------------------------------------

# Evandro C. R. da Rosa
*Quantuloop (Brasil)*

![Evandro C. R. da Rosa](fotos/evandro.jpg)

<p align="justify">
Evandro é co-fundador da Quantuloop, start-up brasileira de computação quântica, e membro do Grupo de Computação Quântica da UFSC (GCQ-UFSC) desde sua criação. Mestre em Ciência da Computação pela Universidade Federal de Santa Catarina (UFSC), em sua dissertação, Evandro desenvolveu uma técnica para interação dinâmica de dados clássicos e quânticos entre computadores clássicos e computadores quânticos em nuvem, trabalho que resultou no projeto de open-source Ket. Sua área de pesquisa abrange programação e simulação quântica, criando ferramentas para facilitar o desenvolvimento de aplicações quânticas e métodos para reduzir o tempo e uso de memória na simulação de computadores quânticos.
</p>

--------------------------------------

# Ivan Santos Oliveira
*Centro Brasileiro de Pesquisas Físicas (Brasil)*

![Ivan Santos Oliveira](fotos/ivan.jpg)

<p align="justify">
Ivan S. Oliveira é um Pesquisador Sênior no Centro Brasileiro de Pesquisas Físicas (CBPF), trabalhando nas áreas de ressonância magnética nuclear e processamento de informações quânticas. Ele publicou cerca de 120 artigos, cinco livros e orientou 15 teses de doutorado e 13 dissertações de mestrado.
</p>

--------------------------------------

# Jonas Maziero
*Universidade Federal de Santa Maria (Brasil)*

![Jonas Maziero](fotos/jonas.jpg)

<p align="justify">
Possui Bacharelado (2007) em Física pela Universidade Federal de Santa Maria (UFSM), Mestrado (2009) e Doutorado (2012) em Física pela Universidade Federal do ABC e Pós-Doutorado pela UFSM (2012) e pela Universidad de la República (2016). Foi Professor Adjunto de Física na Universidade Federal do Pampa de 2012 a 2013. Desde 2013 é Professor de Física e Coordenador do Grupo de Informação Quântica e Fenômenos Emergentes na UFSM. Atualmente é Professor Associado II de Física na UFSM. Tem experiência na área de Física Geral, com ênfase em Física Quântica, Ciência da Informação Quântica, Recursos Quânticos, Inteligência Artificial Clássica e Quântica e Fenômenos Emergentes.
</p>

--------------------------------------

# Marcos César Oliveira
*Universidade Estadual de Campinas (Brasil)*

![Marcos César Oliveira](fotos/marcos.jpg)

<p align="justify">
Possui Doutorado em Física pela Universidade Federal de São Carlos (1999). Realizou estágios de pós-doutorado na Universidade de Queensland-Austrália e foi professor visitante do Institute for Quantum Information Science, na universidade de Calgary entre 2011-2012. Atualmente é professor titular e Diretor associado do Instituto de Física Gleb Wataghin da Universidade Estadual de Campinas. Foi Coordenador de Pós-Graduação do Instituto de Física Gleb Wataghin de 2016 a 2021, Presidente do Fórum de coordenadores de pós-graduação em Física e Astronomia de 2019-2021, Vice-Coordenador do Exame Unificado de Física (EUF) de 2016 a 2019 e coordenador do mesmo de 2019 a Julho de 2021. Durante sua carreira, supervisionou mais de 20 estudantes de doutorado e mestrado, e seu foco geral de pesquisa é sobre como os recursos quânticos podem ser empregados para o desenvolvimento de estratégias de comunicação, computação e detecção com as tecnologias experimentais atuais e suas extensões.
</p>

--------------------------------------

# Marco Cerezo
*Laboratório Nacional de Los Alamos (EUA)*

![Marco Cerezo](fotos/marco.jpg)

<p align="justify">
Marco Cerezo é um físico guatemalteco atualmente cientista na equipe do Laboratório Nacional de Los Alamos. Marco obteve seu doutorado na Universidade de La Plata, em Buenos Aires, Argentina, onde trabalhou com informações quânticas, matéria condensada e fundamentos da mecânica quântica. Atualmente, o trabalho de Marco tem como objetivo compreender teoricamente as capacidades e limitações dos algoritmos variacionais em computadores quânticos near-term. Ele estudou fenômenos como barren plateaus, o efeito do ruído em algoritmos quânticos e propôs diversos algoritmos para aplicações near-term.
</p>

--------------------------------------

# Otto Menegasso
*Centro Latino-Americano de Computação Quântica (Brasil)*

![Otto Menegasso](fotos/otto.jpg)

<p align="justify">
Otto Menegasso Pires é mestre em Inteligência Computacional pela UFSC e doutorando em Modelagem Computacional e Tecnologia Industrial no SENAI CIMATEC. Tem experiência em computação quântica com trabalhos de síntese de circuitos quânticos usando aprendizado por reforço e desenvolvimento de uma heurística para a resolução de problemas NP-completo usando o algoritmo QAOA. Participou do desenvolvimento das ferramentas e testes da linguagem brasileira de programação quântica Ket. Atualmente foca em aplicações para a indústria, redes quânticas e quantum machine learning. Tem participação nos projetos do Centro Latino Americano de Computação Quântica (LAQCC) focados em Aplicações Industriais da Computação Quântica e Computação e Tecnologias Avançadas.
</p>

--------------------------------------

# Stefano Gogioso
*Universidade de Oxford (Reino Unido)*

![Stefano Gogioso](fotos/stefano.jpg)

<p align="justify">
Stefano Gogioso é um matemático e cientista da computação do Reino Unido. Ele obteve seu doutorado em Oxford sob a supervisão de Bob Coecke, desenvolvendo maneiras ilustrativas de falar sobre partículas quânticas e seu comportamento peculiar. Atualmente, ele é professor na Universidade de Oxford, onde ministra cursos avançados sobre computação quântica. Sua pesquisa atual se concentra no desenvolvimento de métodos diagramáticos para computação quântica e no estudo da informação quântica no espaço-tempo.
</p>

--------------------------------------

# Thiago Maciel
*Instituto de Tecnologia e Inovação (Emirados Árabes Unidos)*

![Thiago Maciel](fotos/thiago.jpg)

<!-- <p align="justify">

</p> -->