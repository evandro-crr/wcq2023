---
title: 'Organization'
weight: 65
---

* Prof. Eduardo Inacio Duzzioni, Dr. (UFSC) - Coordinator
* Profª. Jerusa Marchi, Drª. (UFSC) - Coordinator
* Prof. Paulo Mafra, Dr. (UFSC)
* Prof. Pedro Castellucci, Dr. (UFSC)
* Evandro Chagas Ribeiro da Rosa, Me. (Quantuloop)
* João Pedro Engster, Me. (UFSC)
* Otto Menegasso Pires, Me. (LAQCC/SENAI CIMATEC)
* Eduardo Willwock Lussi, B.Sc. (UFSC)
* Letícia Bertuzzi, B.Sc. (UFSC)
* César Freitas (UFSC)
* Gabriel Medeiros Lopes (UFSC)
* Gabriel Turatti (UFSC)