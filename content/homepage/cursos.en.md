---
title: 'Minicourses'
weight: 40
header_menu: true
---

------------

# Introduction to quantum programming with Ket 
[*Evandro C. R. da Rosa*](#evandro-c-r-da-rosa)

<p align='justify'>
We can use superposition and entanglement to develop applications accelerated by quantum computers to solve some problems faster than any supercomputer ever could. Although quantum computers capable of outperforming classical computers in solving real-world problems are not yet a reality, we hope they will be ready soon. Until then, we can prepare for that future by developing and testing quantum computing-accelerated solutions today. In this mini-course we will present the main concepts of quantum computing applied in the quantum programming language Ket. We expect all participants to interact during the course, expressing their doubts and testing what they have learned.
</p>

<iframe width="100%" height="315" src="https://www.youtube.com/embed/Mm7V7rbPJg8?si=1bT1blIOWXS9Dlx4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="100%" height="315" src="https://www.youtube.com/embed/9aVBMhCBczE?si=Ab0XedoerrCLLkY0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

------------

# Exploring Quantum Optimization: Enhancing Efficiency in Solving QUBO-Type Combinatorial Optimization Problems
[*Marcos César Oliveira*](#marcos-césar-oliveira)

<p align='justify'>
In this lecture, we will examine the potential of quantum computers to substantially enhance efficiency in solving Quadratic Unconstrained Binary Optimization (QUBO) combinatorial optimization problems. Starting with an accessible explanation of the QUBO concept and its significance in various domains such as logistics, resource planning, machine learning, and image processing, we will explore how quantum computers excel in approaching combinatorial optimization problems. We will present the natural ways in which quantum resources enable solution exploration in optimization problems. Specifically, we will delve into the transposition of QUBO problems into the quantum context and investigate the practical application of the QAOA and FALQON algorithms. Our analysis will encompass the potential benefits of these strategies, including potential improvements in solution efficiency and quality.
</p>

<iframe width="100%" height="315" src="https://www.youtube.com/embed/gx1uD-rB4FI?si=0sg6QbRIwcDgKLXj" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>