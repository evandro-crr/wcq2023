---
title: 'Lightning Talks'
weight: 45
header_menu: true
---

Desbrave em ritmo acelerado com as Lightning Talks! Palestras de 15 minutos sobre computação quântica, seguidas por 5 minutos de perguntas. Uma explosão de conhecimento e diálogo em pouco tempo!

### Programação

Segunda-feira (18/09)

* 16h30 - Design de Portas Lógicas Quânticas. Eduardo Lussi, mestrando em Ciência da Computação - UFSC.
* 16h50 - Expressividade versus Concentração na Função Custo. Lucas Friedrich, doutorando em Física - UFSM.

Quarta-feira (20/09)

* 16h30 - Simulação de Canais Quânticos Ruidosos via Preparação de Estados Quânticos. Prof. Marcelo S. Zanetti - UFSM.
* 16h50 - Simulação de POVM's através de algoritmos quânticos de preparação de estados. Dr. Douglas F. Pinto - UFSM.

<style>
    body { text-align: justify }
</style>