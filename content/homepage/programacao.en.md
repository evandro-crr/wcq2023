---
title: 'Schedule'
weight: 20
header_menu: true
---

<div style="overflow: auto;">
    <table class="table programacao" style="color: black; margin-bottom: 0px;">
        <thead class="">
            <tr>
                <th class="tamanho-coluna-horas"></th>
                <th class="tamanho-coluna-semana">Monday (18/09)</th>
                <th class="tamanho-coluna-semana">Tuesday (19/09)</th>
                <th class="tamanho-coluna-semana">Wednesday (20/09)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>9h</th>
                <td rowspan="3">
                    <a href="#sobre">Opening</a>
                    <br><br><br><br><br>
                    <a href="#introduction-to-quantum-programming-with-Ket">
                        Introduction to quantum programming with Ket - Part 1 (Evandro)
                    </a>
                </td>
                <td>
                    <a href="#using-quantum-computers-for-simulating-fundamental-experiments-of-quantum-computing-and-quantum-mechanics">
                        Using Quantum Computers for Simulating Fundamental Experiments of Quantum Computing and Quantum Mechanics (Jonas Maziero)
                    </a>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>10h</th>
                <!-- <td></td> -->
                <td>
                    <a href="#superconducting-quantum-computing">Superconducting Quantum Computing (Ivan Santos)</a>
                </td>
                <td rowspan="2">
                    <a href="#quantum-podcast">*Quantum Podcast</a>
                </td>
            </tr>
            <tr>
                <th>11h</th>
                <!-- <td></td> -->
                <td>
                    <a href="#diagrammatic-reasoning-for-quantum-computing">*Diagrammatic Reasoning for Quantum Computing (Stefano)</a>
                </td>
                <!-- <td></td> -->
            </tr>
            <tr>
                <th>12h</th>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>13h30</th>
                <td>
                    <a href="#posters-exhibition">Posters Exhibition</a>
                </td>
                <td>
                    <a href="#posters-exhibition">Posters Exhibition</a>
                </td>
                <td>
                    <a href="#posters-exhibition">Posters Exhibition</a>
                </td>
            </tr>
            <tr>
                <th>14h</th>
                <td rowspan="2">
                    <a href="#introduction-to-quantum-programming-with-ket">
                        Introduction to quantum programming with Ket - Part 2 (Evandro)
                    </a>
                </td>
                <td>
                    <a href="#geometric-quantum-machine-learning">*Geometric Quantum Machine Learning (Marco Cerezo)</a>
                </td>
                <td rowspan="2">
                    <a href="#exploring-quantum-optimization:-enhancing-efficiency-in-solving-qubo-type-combinatorial-optimization-problems">
                        Exploring Quantum Optimization: Enhancing Efficiency in Solving QUBO-Type Combinatorial Optimization Problems (Marcos César)
                    </a>
                </td>
            </tr>
            <tr>
                <th>15h</th>
                <!-- <td></td> -->
                <td>
                    <a href="#quantum-state-initialization">*Quantum State Initialization (Adenilton)</a>
                </td>
                <!-- <td></td> -->
            </tr>
            <tr>
                <th>16h</th>
                <td>
                    Break/<a href="#posters-exhibition">Posters Exhibition</a>
                </td>
                <td>
                    Break/<a href="#posters-exhibition">Posters Exhibition</a>
                </td>
                <td>
                    Break/<a href="#posters-exhibition">Posters Exhibition</a>
                </td>
            </tr>
            <tr>
                <th>16h30</th>
                <td>
                    <a href="#lightning-talks">Lightning Talks</a>
                </td>
                <td>
                    <a href="#quantum-initiatives-in-brazil-and-the-global-landscape">
                        Quantum Initiatives in Brazil and the Global Landscape (Celso Villas-Boas)
                    </a>
                </td>
                <td>
                    <a href="#lightning-talks">Lightning Talks</a>
                    <br><br>
                    Closure
                </td>
            </tr>
        </tbody>
    </table>
    *Remote activity
</div>

<style>

    .tamanho-coluna-horas {
        min-width: 55px;
    }

    .tamanho-coluna-semana {
        min-width: 190px;
    }

    .programacao > thead > tr > th, .programacao > tbody > tr > th {
        background-color: #26a69a !important;
        border: 0 !important;
    }

    td {
        border: 0 !important;
        vertical-align: middle !important;
        word-wrap: normal !important;
    }

    td > a {
        text-decoration: none;
    }

    td > a:hover {
        color: black;
        font-weight: bold;
    }

    table tbody > tr:nth-child(odd) > td, table tbody > tr:nth-child(odd) > th {
        background-color: white;
    }

    table tbody > tr:nth-child(even) > td, table tbody > tr:nth-child(even) > th {
        background-color: #ddf2f0;
    }

</style>

All in person activities will take place in the João Ernesto Escosteguy Castro Auditorium (EPS Auditorium).

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d220.9849840858154!2d-48.51830482017248!3d-27.600977318324933!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9527390087cdc613%3A0x6eafac2391c847f0!2sAudit%C3%B3rio%20Jo%C3%A3o%20Ernesto%20Escosteguy%20Castro!5e0!3m2!1en-US!2sbr!4v1692969515514!5m2!1sen-US!2sbr" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
