---
title: 'Talks'
weight: 30
header_menu: true
---

-------------------------------

# Quantum State Initialization

[*Adenilton José da Silva*](#adenilton-jose-da-silva)

<p align="justify">
Omnipresent in quantum computing is the step to encode data into a quantum state. This process is called quantum state preparation, and its complexity for unstructured data is exponential in the number of qubits. Several works address this problem, for example, using variational methods that train a fixed-depth circuit with manageable complexity. These methods have their limitations, such as the lack of a backpropagation technique and plateaus. In this talk, an algorithm will be described to reduce the depth of the state preparation circuit by offloading the computational complexity to a classical computer. The initialized quantum state can be exact or an approximation, and we show that the approximation performs better on current quantum processors than initializing the original state. Experimental evaluation demonstrates that the proposed method allows for a more efficient initialization of probability distributions in a quantum state.
</p>

*Remote activity.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/63RPxPywNuY?si=ocxHVrjZ3N_57Fe3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

-------------------------------

# Using Quantum Computers for Simulating Fundamental Experiments of Quantum Computing and Quantum Mechanics

[*Jonas Maziero*](#jonas-maziero)

<p align="justify">
Experiments on the fundamentals of quantum mechanics and quantum communication are generally very expensive. This prevents many research and teaching groups (RTGs) from conducting such experiments. Universal quantum computers can be used to implement any quantum state or dynamics. With the availability of quantum computers in the cloud, it has become possible to simulate these experiments at an affordable cost for Brazilian RTGs. This even encourages the proposition and development of new experiments. In this seminar, I will start by explaining the basic elements of quantum computing and its implementation using Qiskit as a programming tool to experiment with IBM's quantum computers, which can be freely accessed in the cloud. We will use these computers to simulate experiments on the fundamentals of quantum mechanics (Bell inequalities violation, Mach-Zehnder interferometer, etc.) and quantum communication (quantum teleportation, entanglement swapping, etc.).
</p>

<iframe width="100%" height="315" src="https://www.youtube.com/embed/vv-YZgw8bm8?si=Zsta2bMxWJk9sHj8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

-------------------------------

# Geometric Quantum Machine Learning

[*Marco Cerezo*](#marco-cerezo)

<p align="justify">
Quantum Machine Learning (QML) models aim at learning from data encoded in quantum states. Recently, it has been shown that models with little to no inductive biases are likely to have trainability and generalization issues, especially when trying to scale them to real world problem sizes. As such, it is fundamental to develop models that encode as much information, or assumptions, as available about the problem at hand. This often untapped source of information comes predominantly as knowledge of the symmetries of the underlying problem. Inspired by insights and successes found in the field of geometric deep learning, we recognize symmetries as a central aspect in the development of QML frameworks. In this talk we present theory grounding the design of geometric QML models, methodology to construct and parameterize such models, and illustrate their advantages in concrete examples.
</p>

*In English.

**Remote activity.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/4QckGu1YjJg?si=PBdUMQdwbV2LYZGQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

-------------------------------

# Superconducting Quantum Computing

[*Ivan Santos*](#ivan-santos)

<p align="justify">
Superconducting devices have shown to be the most promising technology for scaling quantum computing (QC). This seminar will present the principles of QC in superconducting chips, the state of the art in the development of this technology in Brazil and the ongoing installation of an open laboratory for the construction of superconducting devices for QC at CBPF.
</p>

<iframe width="100%" height="315" src="https://www.youtube.com/embed/-SmbFnJdI0s?si=NplRleifZ6SCzzYM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


-------------------------------

# Diagrammatic Reasoning for Quantum Computing

[*Stefano Gogioso*](#stefano-gogioso)

<p align="justify">
The ZX calculus is a graphical language that provides an intuitive and elegant way to reason about quantum computing, rigorously backed by over 200 research papers (cf. zxcalculus.com/publications). ZX diagrams will initially look like quantum circuits, making them familiar to those already in this field. Unlike quantum circuits, however, these pictures are more than mere schematics: they are a new kind of sophisticated and rigorous mathematics, tailor-made to talk about the quantum world. In this presentation, I will introduce the ZX calculus's fundamental concepts and techniques in a manner accessible to quantum computing enthusiasts and professionals alike. I will then proceed to apply the calculus to picturing some topical quantum applications, such as Measurement-Based Quantum Computing and the Quantum Approximate Optimization Algorithm.
</p>

*In English.

**Remote activity.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/uo5rYWZgOZU?si=PpA4pE7wLotFqeUp" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

-------------------------------

# Quantum Initiatives in Brazil and the Global Landscape

[*Celso Villas-Boas*](#celso-villas-boas)

<p align="justify">
In this lecture, we will delve into the realm of second-generation quantum technologies, which hold tremendous potential to revolutionize information processing, transmission, and acquisition technologies on a global scale. Due to their disruptive power, substantial investments have been pouring in from governments and the private sector worldwide, with this trend persisting systematically for at least five years. Conversely, it is only within the past two years that this topic has begun to gain traction in Brazil, with a focus on establishing robust, long-term quantum technology programs.
</p>

<iframe width="100%" height="315" src="https://www.youtube.com/embed/unNL-TxlsI4?si=uuT1BXpidCnUZLD2" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>