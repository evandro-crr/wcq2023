---
title: 'Programação'
weight: 20
header_menu: true
---

<div style="overflow: auto;">
    <table class="table programacao" style="color: black; margin-bottom: 0px;">
        <thead class="">
            <tr>
                <th class="tamanho-coluna-horas"></th>
                <th class="tamanho-coluna-semana">Segunda-feira (18/09)</th>
                <th class="tamanho-coluna-semana">Terça-feira (19/09)</th>
                <th class="tamanho-coluna-semana">Quarta-feira (20/09)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>9h</th>
                <td rowspan="3">
                    <a href="#sobre">Abertura do Evento</a>
                    <br><br><br><br><br>
                    <a href="#introdução-à-programação-quântica-com-ket">
                        Introdução à programação quântica com Ket - Parte 1 (Evandro)
                    </a>
                </td>
                <td>
                    <a href="#usando-computadores-quânticos-para-simular-experimentos-fundamentais-de-mecânica-quântica-e-comunicação-quântica">
                        Usando Computadores Quânticos para Simular Experimentos Fundamentais de Mecânica Quântica e Comunicação Quântica (Jonas Maziero)
                    </a>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>10h</th>
                <!-- <td></td> -->
                <td>
                    <a href="#computadores-quânticos-supercondutores">Computadores Quânticos Supercondutores (Ivan Santos)</a>
                </td>
                <td rowspan="2">
                    <a href="#podcast-quântico">*Podcast Quântico</a>
                </td>
            </tr>
            <tr>
                <th>11h</th>
                <!-- <td></td> -->
                <td>
                    <a href="#raciocínio-diagramático-para-computação-quântica">*Raciocínio Diagramático para Computação Quântica (Stefano)</a>
                </td>
                <!-- <td></td> -->
            </tr>
            <tr>
                <th>12h</th>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>13h30</th>
                <td>
                    <a href="#exposição-de-pôsteres">Exposição de Pôsteres</a>
                </td>
                <td>
                    <a href="#exposição-de-pôsteres">Exposição de Pôsteres</a>
                </td>
                <td>
                    <a href="#exposição-de-pôsteres">Exposição de Pôsteres</a>
                </td>
            </tr>
            <tr>
                <th>14h</th>
                <td rowspan="2">
                    <a href="#introdução-à-programação-quântica-com-ket">
                        Introdução à programação quântica com Ket - Parte 2 (Evandro)
                    </a>
                </td>
                <td>
                    <a href="#aprendizado-de-máquina-quântico-geométrico">*Aprendizado de Máquina Quântico Geométrico (Marco Cerezo)</a>
                </td>
                <td rowspan="2">
                    <a href="#explorando-a-otimização-quântica:-potencializando-a-eficiência-na-resolução-de-problemas-de-otimização-combinatorial-do-tipo-qubo">
                        Explorando a Otimização Quântica: Potencializando a Eficiência na Resolução de Problemas de Otimização Combinatorial do Tipo QUBO (Marcos César)
                    </a>
                </td>
            </tr>
            <tr>
                <th>15h</th>
                <!-- <td></td> -->
                <td>
                    <a href="#inicialização-de-estados-quânticos">*Inicialização de Estados Quânticos (Adenilton)</a>
                </td>
                <!-- <td></td> -->
            </tr>
            <tr>
                <th>16h</th>
                <td>
                    Intervalo/<a href="#exposição-de-pôsteres">Exposição de Pôsteres</a>
                </td>
                <td>
                    Intervalo/<a href="#exposição-de-pôsteres">Exposição de Pôsteres</a>
                </td>
                <td>
                    Intervalo/<a href="#exposição-de-pôsteres">Exposição de Pôsteres</a>
                </td>
            </tr>
            <tr>
                <th>16h30</th>
                <td>
                    <a href="#lightning-talks">Lightning Talks</a>
                </td>
                <td>
                    <a href="#iniciativas-quânticas-no-brasil-e-cenário-mundial">
                        Iniciativas quânticas no Brasil e cenário mundial (Celso Villas-Boas)
                    </a>
                </td>
                <td>
                    <a href="#lightning-talks">Lightning Talks</a>
                    <br><br>
                    Encerramento
                </td>
            </tr>
        </tbody>
    </table>
    *Atividade remota.
</div>

<style>

    .tamanho-coluna-horas {
        min-width: 55px;
    }

    .tamanho-coluna-semana {
        min-width: 190px;
    }

    .programacao > thead > tr > th, .programacao > tbody > tr > th {
        background-color: #26a69a !important;
        border: 0 !important;
    }

    td {
        border: 0 !important;
        vertical-align: middle !important;
        word-wrap: normal !important;
    }

    td > a {
        text-decoration: none;
    }

    td > a:hover {
        color: black;
        font-weight: bold;
    }

    table tbody > tr:nth-child(odd) > td, table tbody > tr:nth-child(odd) > th {
        background-color: white;
    }

    table tbody > tr:nth-child(even) > td, table tbody > tr:nth-child(even) > th {
        background-color: #ddf2f0;
    }

</style>


Todas as atividades presenciais ocorrerão no Auditório João Ernesto Escosteguy Castro (Auditório do EPS).

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d220.9849840858154!2d-48.51830482017248!3d-27.600977318324933!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9527390087cdc613%3A0x6eafac2391c847f0!2sAudit%C3%B3rio%20Jo%C3%A3o%20Ernesto%20Escosteguy%20Castro!5e0!3m2!1spt-BR!2sbr!4v1692969515514!5m2!1spt-BR!2sbr" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
