---
title: 'Lightning Talks'
weight: 45
header_menu: true
---

Explore at an accelerated pace with our Lightning Talks! 15-minute lectures on quantum computing, followed by 5 minutes of questions. A burst of knowledge and dialogue in a short span of time!

### Schedule

Monday (09/18)

* 4:30 PM - Quantum Gates Design. Eduardo Lussi, Master's student in Computer Science - UFSC.
* 4:50 PM - Expressiveness vs. Concentration in Cost Function. Lucas Friedrich, Ph.D. student in Physics - UFSM.

Wednesday (09/20)

* 4:30 PM - Simulation of Noisy Quantum Channels via Quantum State Preparation. Prof. Marcelo S. Zanetti - UFSM.
* 4:50 PM - Simulation of POVMs through Quantum State Preparation Algorithms. Dr. Douglas F. Pinto - UFSM.

<style>
    body { text-align: justify }
</style>