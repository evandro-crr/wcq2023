---
title: 'Palestras'
weight: 30
header_menu: true
---

-------------------------------

# Inicialização de Estados Quânticos

[*Adenilton José da Silva*](#adenilton-jose-da-silva)

<p align="justify">
Onipresente na computação quântica é o passo para codificar dados em um estado quântico. Este processo é chamado de preparação de estado quântico, e sua complexidade para dados não estruturados é exponencial no número de qubits. Vários trabalhos abordam esse problema, por exemplo, usando métodos variacionais que treinam um circuito de profundidade fixa com complexidade gerenciável. Esses métodos têm suas limitações, como a falta de uma técnica de retro propagação e platôs. Nesta palestra será descrito um algoritmo para reduzir a profundidade do circuito de preparação de estado descarregando a complexidade computacional para um computador clássico. O estado quântico inicializado pode ser exato ou uma aproximação, e mostramos que a aproximação é melhor nos processadores quânticos atuais do que a inicialização do estado original. A avaliação experimental demonstra que o método proposto permite uma inicialização mais eficiente de distribuições de probabilidade em um estado quântico.
</p>

*Atividade remota.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/63RPxPywNuY?si=ocxHVrjZ3N_57Fe3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

-------------------------------

# Usando Computadores Quânticos para Simular Experimentos Fundamentais de Mecânica Quântica e Comunicação Quântica

[*Jonas Maziero*](#jonas-maziero)

<p align="justify">
Experimentos sobre fundamentos de mecânica quântica e comunicação quântica são, em geral, muito caros. Isso impossibilita que muitos grupos de pesquisa e ensino (GPE) realizem tais experimentos. Computadores quânticos universais podem ser usados para implementar qualquer estado ou dinâmica quântica. Com a disponibilidade de computadores quânticos na nuvem, tornou-se possível simular esses experimentos a um custo acessível a GPE brasileiros. Isso estimula inclusive a proposição e desenvolvimento de novos experimentos. Nesse seminário, começarei explicando os elementos básicos da computação quântica e sua implementação usando o Qiskit como ferramenta de programação para experimentar com os computadores quânticos da IBM, que podem ser acessados livremente na nuvem. Usaremos esses computadores para simular experimentos sobre os fundamentos de mecânica quântica (violação de desigualdades de Bell, interferômetro de Mach-Zehnder, etc) e de comunicação quântica (teleportação quântica, swap de emaranhamento, etc).
</p>

<iframe width="100%" height="315" src="https://www.youtube.com/embed/vv-YZgw8bm8?si=Zsta2bMxWJk9sHj8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

-------------------------------

# Aprendizado de Máquina Quântico Geométrico

[*Marco Cerezo*](#marco-cerezo)

<p align="justify">
Os modelos de Aprendizado de Máquina Quântico (QML) têm como objetivo aprender a partir de dados codificados em estados quânticos. Recentemente, foi demonstrado que modelos com poucos ou nenhum viés indutivo são propensos a ter problemas de treinamento e generalização, especialmente ao tentar dimensioná-los para tamanhos de problemas do mundo real. Portanto, é fundamental desenvolver modelos que codifiquem o máximo de informações ou suposições disponíveis sobre o problema em questão. Essa fonte frequentemente subutilizada de informações surge principalmente do conhecimento das simetrias do problema subjacente. Inspirados por insights e sucessos encontrados no campo do aprendizado profundo geométrico, reconhecemos as simetrias como um aspecto central no desenvolvimento de estruturas de QML. Nesta apresentação, apresentamos teoria fundamentando o design de modelos geométricos de QML, metodologia para construir e parametrizar tais modelos e ilustramos suas vantagens em exemplos concretos.
</p>

*Palestra em inglês.

**Atividade remota.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/4QckGu1YjJg?si=PBdUMQdwbV2LYZGQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


-------------------------------

# Computadores Quânticos Supercondutores

[*Ivan Santos*](#ivan-santos)

<p align="justify">
Dispositivos supercondutores têm se mostrado a tecnologia mais promissora para a escalabilidade da computação quântica (QC). Neste seminário, serão apresentados os princípios da QC em chips supercondutores, o estado da arte no desenvolvimento dessa tecnologia no Brasil e a instalação em andamento de um laboratório aberto para a construção de dispositivos supercondutores para QC no CBPF.
</p>

<iframe width="100%" height="315" src="https://www.youtube.com/embed/-SmbFnJdI0s?si=NplRleifZ6SCzzYM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

-------------------------------

# Raciocínio Diagramático para Computação Quântica

[*Stefano Gogioso*](#stefano-gogioso)

<p align="justify">
O ZX-calculus é uma linguagem gráfica que oferece uma maneira intuitiva e elegante de raciocinar sobre computação quântica, respaldada de forma rigorosa por mais de 200 artigos de pesquisa. Os diagramas ZX inicialmente se assemelham a circuitos quânticos, tornando-os familiares para aqueles que já estão nesse campo. No entanto, ao contrário dos circuitos quânticos, essas representações são mais do que meros esquemas: elas constituem um novo tipo de matemática sofisticada e rigorosa, feita sob medida para tratar do mundo quântico. Nesta apresentação, introduzirei os conceitos fundamentais e técnicas do ZX-calculus de uma forma acessível tanto para entusiastas quanto profissionais da computação quântica. Em seguida, aplicarei o cálculo para representar algumas aplicações quânticas atuais, como a Computação Quântica Baseada em Medição e o Algoritmo de Otimização Aproximada Quântica.
</p>

*Palestra em inglês.

**Atividade remota.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/uo5rYWZgOZU?si=PpA4pE7wLotFqeUp" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

-------------------------------

# Iniciativas quânticas no Brasil e cenário mundial

[*Celso Villas-Boas*](#celso-villas-boas)

<p align="justify">
Nesta palestra vamos discutir um pouco sobre as tecnologias quânticas de segunda geração, que tem um potencial gigantesco de revolucionar as tecnologias ligadas ao processamento, transmissão e captação de informações em geral. Por conta de seu poder disruptivo, já há investimentos vultuosos ao redor do mundo, tanto por parte de governos quanto do setor privado, e que vem acontecendo de forma sistemática há pelo menos 5 anos. Por outro lado, somente há uns dois anos é que o assunto começou a ser discutido no Brasil, visando a criação de programas de tecnologias quânticas robustos e com visão de longo prazo.
</p>

<iframe width="100%" height="315" src="https://www.youtube.com/embed/unNL-TxlsI4?si=uuT1BXpidCnUZLD2" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>