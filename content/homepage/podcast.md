---
title: 'Podcast Quântico'
weight: 47
header_menu: true
---

Em nosso Podcast Quântico, alunos e ex-alunos a compartilharão suas experiências e perspectivas sobre as carreiras que se desenrolam após a graduação ou pós-graduação na área de computação quântica. Junte-se a nós em conversas autênticas enquanto discutimos trajetórias profissionais, desafios enfrentados e os momentos que definem o caminho para o sucesso na indústria quântica. Se você está curioso sobre as possibilidades do mundo quântico ou está considerando suas próprias opções de carreira, este podcast oferece insights valiosos diretamente de quem está trilhando esse percurso!

Participantes:

- [*Bruno Taketani*](#bruno-taketani)

- [*Evandro C. R. da Rosa*](#evandro-c-r-da-rosa)

- [*Otto Menegasso*](#otto-menegasso)

- [*Thiago Maciel*](#thiago-maciel)

<style>
    body { text-align: justify }
</style>

<iframe width="100%" height="315" src="https://www.youtube.com/embed/Mhxq8O1RsEc?si=jPU1kMtKAJ0AV1NE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>