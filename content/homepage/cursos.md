---
title: 'Minicursos'
weight: 40
header_menu: true
---

------------

# Introdução à programação quântica com Ket 
[*Evandro C. R. da Rosa*](#evandro-c-r-da-rosa)

<p align='justify'>
Podemos usar superposição e emaranhamento para desenvolver aplicações aceleradas por computadores quânticos para resolver alguns problemas mais rápido do que qualquer supercomputador jamais poderia. Embora computadores quânticos capazes de superar computadores clássicos na resolução de problemas do mundo real ainda não sejam uma realidade, esperamos que eles estejam prontos em breve. Até lá, já podemos nos preparar para esse futuro, desenvolvendo e testando soluções aceleradas pela computação quântica hoje. Neste minicurso apresentaremos os principais conceitos da computação quântica aplicadas na linguagem de programação quântica Ket. Esperamos que todos os participantes interajam durante o curso, manifestando suas dúvidas e testando o que aprenderam.
</p>

<iframe width="100%" height="315" src="https://www.youtube.com/embed/Mm7V7rbPJg8?si=1bT1blIOWXS9Dlx4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="100%" height="315" src="https://www.youtube.com/embed/9aVBMhCBczE?si=Ab0XedoerrCLLkY0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

------------

# Explorando a Otimização Quântica: Potencializando a Eficiência na Resolução de Problemas de Otimização Combinatorial do Tipo QUBO
[*Marcos César Oliveira*](#marcos-césar-oliveira)

<p align='justify'>
Nesta palestra, examinaremos a capacidade dos computadores quânticos de substancialmente aprimorar a eficiência na resolução de problemas de otimização combinatorial do tipo QUBO (Quadratic Unconstrained Binary Optimization). Iniciando com uma explanação acessível do conceito de QUBO e sua importância em diversas áreas, como logística, planejamento de recursos, aprendizado de máquina e processamento de imagens, exploraremos como os computadores quânticos se destacam na abordagem de problemas de otimização combinatorial. Apresentaremos a maneira natural pela qual os recursos quânticos permitem a exploração de soluções em problemas de otimização. De maneira mais específica, abordaremos a transposição de problemas QUBO para o contexto quântico e investigaremos a aplicação prática dos algoritmos QAOA e FALQON. Nossa análise compreenderá os potenciais benefícios dessas estratégias, incluindo possíveis melhorias na eficiência e qualidade das soluções obtidas.
</p>

<iframe width="100%" height="315" src="https://www.youtube.com/embed/gx1uD-rB4FI?si=0sg6QbRIwcDgKLXj" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>