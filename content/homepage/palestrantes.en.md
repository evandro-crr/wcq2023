---
title: 'Speakers'
weight: 50
header_menu: true 
---

--------------------------------------

# Adenilton José da Silva
*Federal University of Pernambuco (Brazil)*

![Adenilton José da Silva](../fotos/adenilton.jpg)

<p align="justify">
Adenilton Silva has been a professor at the Center for Informatics at UFPE since 2019. His main research interests include system software for quantum devices, quantum machine learning, and algorithms.
</p>

--------------------------------------

# Bruno Taketani
*IQM Quantum Computers (Germany)*

![Bruno Taketani](../fotos/bruno.jpg)

<p align="justify">
Bruno G. Taketani works at IQM Quantum Computers as a Technical Product Manager developing the integration of the company’s system into HPC environments. His work on quantum computing started in his Ph.D. focused on the development of NISQ-era protocols. This was followed by 5 years as a researcher in Germany working on quantum algorithms and applications. Before joining IQM, Bruno held a professorship at UFSC, Brazil, where he helped strengthen the local efforts in quantum technologies. Bruno also works building the European ecosystem in the European Quantum Industry Consortium (QuIC) and represents this group in an advisory group to the EuroHPC Joint Undertaken.
</p>

--------------------------------------

# Celso Villas-Boas
*Federal Univerisity of São Carlos (Brazil)*

![Celso Villas-Boas](../fotos/celso.jpg)

<p align="justify">
Celso J. Villas-Boas is an Associate Professor at UFSCar, São Carlos. He has served as the Head of the Physics Department and as the Coordinator of both undergraduate and graduate programs in Physics at UFSCar. With over 25 years of experience in Optics and Quantum Information, his expertise includes quantum information processing, quantum computing, and radiation-matter interactions. He has supervised over 50 research projects at the undergraduate, master's, doctoral, and postdoctoral levels, and has co-authored significant contributions to the field. For instance, he was the first to demonstrate the robustness of quantum discord, the first to demonstrate electromagnetically induced transparency with individual atoms, and the first to show that the generation of correlations between atoms serves as a witness of the non-classicality of intense optical fields. Currently, he leads projects in second-generation quantum technologies in collaboration with public and private organizations.
</p>

--------------------------------------

# Evandro C. R. da Rosa
*Quantuloop (Brazil)*

![Evandro C. R. da Rosa](../fotos/evandro.jpg)

<p align="justify">
Evandro is a co-founder of Quantuloop, a Brazilian quantum computing start-up, and a member of the UFSC Quantum Computing Group (GCQ-UFSC) since its inception. Master in Computer Science from the Federal University of Santa Catarina (UFSC), in his dissertation, Evandro developed a technique for dynamic interaction of classical and quantum data between classical computers and quantum computers in the cloud, work that resulted in the open-source project Ket . His area of research encompasses quantum programming and simulation, creating tools to facilitate the development of quantum applications and methods to reduce time and memory usage in the simulation of quantum computers.
</p>

--------------------------------------

# Ivan Santos Oliveira
*Brazilian Center for Research in Physics (Brasil)*

![Ivan Santos Oliveira](../fotos/ivan.jpg)

<p align="justify">
Ivan S. Oliveira is a Senior Researcher at the Brazilian Center for Physical Research (CBPF), working in the areas of nuclear magnetic resonance and quantum information processing. He has published around 120 articles, five books and has supervised 15 doctoral theses and 13 master's dissertations.
</p>

--------------------------------------

# Jonas Maziero
*Federal University of Santa Maria (Brazil)*

![Jonas Maziero](../fotos/jonas.jpg)

<p align="justify">
Holds a Bachelor's degree (2007) in Physics from the Federal University of Santa Maria (UFSM), a Master's degree (2009) and a Ph.D. (2012) in Physics from the Federal University of ABC, and completed Postdoctoral studies at UFSM (2012) and Universidad de la República (2016). From 2012 to 2013, they were an Adjunct Professor of Physics at the Federal University of Pampa. Since 2013, they have been a Professor of Physics and Coordinator of the Quantum Information and Emergent Phenomena Group at UFSM. Currently, they hold the position of Associate Professor II of Physics at UFSM. Their expertise lies in General Physics, with a focus on Quantum Physics, Quantum Information Science, Quantum Resources, Classical and Quantum Artificial Intelligence, and Emergent Phenomena.
</p>

--------------------------------------

# Marcos César Oliveira
*State University of Campinas (Brazil)*

![Marcos César Oliveira](../fotos/marcos.jpg)

<p align="justify">
Holds a Ph.D. in Physics from the Federal University of São Carlos (1999). Conducted postdoctoral research at the University of Queensland, Australia, and was a visiting professor at the Institute for Quantum Information Science, University of Calgary, between 2011-2012. Currently, He is a full professor and Associate Director of the Gleb Wataghin Institute of Physics at the State University of Campinas. He served as the Graduate Program Coordinator at the Gleb Wataghin Institute of Physics from 2016 to 2021, President of the Forum of Graduate Program Coordinators in Physics and Astronomy from 2019 to 2021, Vice-Coordinator of the Unified Physics Examination (EUF) from 2016 to 2019, and Coordinator of the same from 2019 to July 2021. Throughout his career, he has supervised more than 20 Ph.D. and master's students, and his general research focus is on how quantum resources can be employed for the development of communication, computation, and detection strategies with current experimental technologies and their extensions.
</p>

--------------------------------------

# Marco Cerezo
*Los Alamos National Laboratory (USA)*

![Marco Cerezo](../fotos/marco.jpg)

<p align="justify">
Marco Cerezo is a Guatemalan physicist currently employed as a staff scientist at Los Alamos National Laboratory. Marco obtained his PhD at the university of La Plata, in Buenos Aires, Argentina, where he was working on quantum information, condensed matter, and foundations of quantum mechanics. Currently, Marco’s work is aimed at theoretically understanding the capabilities and limitations of variational algorithms on near-term quantum computers. Here, he has studied phenomena such as barren plateaus, the effect of noise on quantum algorithms, and has proposed several algorithms for near-term applications.
</p>

--------------------------------------

# Otto Menegasso
*Latin America Quantum Computing Center (Brazil)*

![Otto Menegasso](../fotos/otto.jpg)

<p align="justify">
Otto Menegasso Pires holds a Master's degree in Computational Intelligence from UFSC and is currently pursuing a Ph.D. in Computational Modeling and Industrial Technology at SENAI CIMATEC. He has experience in quantum computing, with research involving quantum circuit synthesis using reinforcement learning and the development of a heuristic for solving NP-complete problems using the QAOA algorithm. He has also contributed to the development and testing of the Brazilian quantum programming language Ket. Currently, his focus is on applications for industry, quantum networks, and quantum machine learning. He is actively involved in projects at the Latin American Center for Quantum Computing (LAQCC) that center around Industrial Applications of Quantum Computing and Advanced Computing and Technologies.
</p>

--------------------------------------

# Stefano Gogioso
*Oxford University (United Kingdom)*

![Stefano Gogioso](../fotos/stefano.jpg)

<p align="justify">
Stefano Gogioso is a UK mathematician and computer scientist. He got his PhD in Oxford under the supervision of Bob Coecke, developing pictorial ways to talk about quantum particles and their peculiar behavior. He is now a lecturer at the University of Oxford, where he holds advanced courses on quantum computing. His current research focuses on the development of diagrammatic methods for quantum computing and the study of quantum information in spacetime.
</p>

--------------------------------------

# Thiago Maciel
*Technology Innovation Institute (United Arab Emirates)*

![Thiago Maciel](../fotos/thiago.jpg)

<!-- <p align="justify">

</p> -->