---
title: 'Quantum Podcast'
weight: 47
header_menu: true
---

In our Quantum Podcast, students and alumni will share their experiences and perspectives on careers unfolding after graduation or post-graduation in the field of quantum computing. Join us for authentic conversations as we delve into professional journeys, challenges faced, and the defining moments that shape the path to success in the quantum industry. Whether you're curious about the possibilities of the quantum world or contemplating your own career options, this podcast offers valuable insights directly from those treading this path!

Participants:

- [*Bruno Taketani*](#bruno-taketani)

- [*Evandro C. R. da Rosa*](#evandro-c-r-da-rosa)

- [*Otto Menegasso*](#otto-menegasso)

- [*Thiago Maciel*](#thiago-maciel)

<style>
    body { text-align: justify }
</style>

<iframe width="100%" height="315" src="https://www.youtube.com/embed/Mhxq8O1RsEc?si=jPU1kMtKAJ0AV1NE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>