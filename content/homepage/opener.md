---
title: 'Sobre'
weight: 10
---

Com grande entusiasmo, anunciamos a mais recente edição do Workshop de Computação Quântica. Este evento reúne renomados especialistas da academia e indústria, com o propósito de manter a comunidade brasileira atualizada sobre os rápidos avanços no campo da Computação Quântica. Esta revolução tem como base a aplicação da mecânica quântica para a criação de uma nova lógica de processamento de informações.

Apesar de estar em seus estágios iniciais, os processadores quânticos [Jiuzhang](https://www.science.org/doi/10.1126/science.abe8770), [Jiuzhang 2.0](https://doi.org/10.1103/PhysRevLett.127.180502) e [Zuchongzhi](https://doi.org/10.1103/PhysRevLett.127.180501), todos provenientes da China, juntamente com o notável processador fotônico [Borealis](https://www.nature.com/articles/s41586-022-04725-x) desenvolvido pela conceituada empresa canadense Xanadu, já ultrapassaram a capacidade de processamento de supercomputadores de renome, como o Summit e o Sunway TaihuLight. Em alguns casos, os computadores quânticos demonstraram ser até 10 milhões de bilhões de vezes mais velozes do que suas contrapartes clássicas em simulações exatas!

O [Grupo de Computação Quântica da UFSC](http://www.gcq.ufsc.br/) tem a honra de convidá-lo a se juntar a nós nessa empolgante revolução científica e tecnológica. Durante o evento, ofereceremos palestras e minicursos para apresentar as mais recentes conquistas nesse campo dinâmico. Abaixo, detalhamos mais informações sobre o evento e como você pode participar ativamente dessa jornada rumo ao futuro da computação.

<br>

### [Inscreva-se!](#inscrição) (Gratuito)

Acompanhe nosso [Instagram](https://www.instagram.com/gcq_ufsc/) e [Youtube](https://www.youtube.com/@GCQUFSC) não perca a oportunidade de se inscrever e participar desse evento transformador. Esteja preparado para se juntar a nós nessa jornada de descobertas e avanços na Computação Quântica. Sua presença é fundamental para enriquecer ainda mais essa experiência única. Aguardamos ansiosamente por sua inscrição!

<br>

### Como participar

Participar do evento é simples! Tudo o que você precisa fazer é se [inscrever](#inscrição) para garantir seu lugar. Uma vez inscrito, você terá duas opções para acompanhar as palestras:

1. **Ao vivo pelo YouTube:** Acompanhe as palestras de onde quiseres, através de transmissões ao vivo pelo nosso canal oficial no YouTube. Você terá a oportunidade de interagir por meio de comentários e absorver o conhecimento diretamente dos especialistas no campo da Computação Quântica.

2. **Presencialmente:** Se preferir uma experiência mais imersiva, convidamos você a se juntar a nós pessoalmente no local do evento. Confira a programação para obter detalhes sobre os horários e locais das palestras. Estamos ansiosos para recebê-lo em um ambiente inspirador, onde você poderá se conectar diretamente com outros entusiastas da área e mergulhar nas discussões revolucionárias.

Lembre-se de verificar a programação completa para obter informações sobre os horários, locais e palestrantes. Esta é a sua oportunidade de se envolver com a vanguarda da Computação Quântica e ampliar seus horizontes nesse campo em constante evolução. Mal podemos esperar para compartilhar essa jornada com você!

<br>

### Edições Anteriores

À medida que nos preparamos para a emocionante sexta edição do Workshop de Computação Quântica, vale a pena relembrar as edições anteriores que nos trouxeram até aqui:

- [V Workshop de Computação Quântica (2022)](https://workshop-cq.ufsc.br/2022/)
- [IV Workshop de Computação Quântica (2021)](https://workshop-cq.ufsc.br/2021/)
- [III Workshop de Computação Quântica (2020)](https://workshop-cq.ufsc.br/2020/)
- [II Workshop de Computação Quântica (2019)](https://workshop-cq.ufsc.br/2019/)
- [I Workshop de Computação Quântica (2018)](https://workshop-cq.ufsc.br/2018/)

<style>
    body { text-align: justify }
</style>