---
title: 'Posters Exhibition'
weight: 47
header_menu: true
---

The Poster Exhibition provides a valuable opportunity for participants to share their research and discoveries with the academic and professional community. There will be sessions every day from 1:30 PM to 2:00 PM and between 4:00 PM and 4:30 PM. The aim is to create an interactive environment where participants can discuss their projects, findings, and ideas related to quantum computing. It's an excellent networking opportunity!

To present your poster, simply complete the registration form below by Friday, September 15th, and bring your poster on the day of the event.

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdGk1uL6AI5uSSplcOPFY3MzTlvfvtcuzTfCsmD8thRhWm72Q/viewform?embedded=true" width="640" height="1230" frameborder="0" marginheight="0" marginwidth="0">Carregando…</iframe>

<style>
    body { text-align: justify }
</style>