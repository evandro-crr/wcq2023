---
title: 'Exposição de Pôsteres'
weight: 47
header_menu: true
---

A Exposição de Pôsteres é uma oportunidade valiosa para os participantes compartilharem suas pesquisas e descobertas com a comunidade acadêmica e profissional. Haverá sessões todos os dias de 13h30 a 14h e entre 16h e 16h30. A ideia é tornar o evento um ambiente interativo onde os participantes possam discutir seus projetos, resultados e ideias relacionados à computação quântica. É uma excelente oportunidade para networking!

Para apresentar seu pôster, basta preencher o formulário de inscrição abaixo até sexta-feira, dia 15/09, e trazer o seu pôster no dia do evento.

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdGk1uL6AI5uSSplcOPFY3MzTlvfvtcuzTfCsmD8thRhWm72Q/viewform?embedded=true" width="640" height="1230" frameborder="0" marginheight="0" marginwidth="0">Carregando…</iframe>

<style>
    body { text-align: justify }
</style>